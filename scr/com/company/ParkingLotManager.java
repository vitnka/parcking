package com.company;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ParkingLotManager {
    public static void main(String[] args) {
        int carNumber = 1000;
        int busNumber = 5000;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter length of the parking for cars, then for buses:");
        Parking parking = new Parking();
        System.out.println("You can press \n 1. To make turn \n 2. To get more information \n 3. To clear parking");
        while (true) {
            int randomCars = 1 + (int) (Math.random() * parking.getParkingCarPlaces() / 3);
            int randomBuses = 1 + (int) (Math.random() * parking.getParkingBusPlaces() / 3);
            ArrayList<Cars> car = new ArrayList<>(randomCars);
            for (int i = 0; i < randomCars; i++) {
                car.add(i, new Cars(1 + (int) (Math.random() * 10), carNumber++));
            }
            ArrayList<Buses> bus = new ArrayList<>(randomBuses);
            for (int i = 0; i < randomBuses; i++) {
                bus.add(i, new Buses(1 + (int) (Math.random() * 10), busNumber++));
            }

            int command = scanner.nextInt();
            switch (command) {
                case 1:
                    for (int i = 0; i < randomCars; i++) {
                        if (parking.getFreeParkingCarPlaces() == 0) {
                            System.out.println("There are no free places!");
                            break;
                        }
                        parking.setCarOnParkingPlace(car.get(i));
                        System.out.println("Places left: " + parking.getFreeParkingCarPlaces());
                    }
                    for (int i = 0; i < randomBuses; i++) {
                        if ((parking.getFreeParkingCarPlaces() == 0) & (parking.getFreeParkingBusPlaces() == 0)) {
                            System.out.println("There are no free places!");
                            break;
                        }
                        parking.setBusOnParkingPlace(bus.get(i));
                        System.out.println("Places left:  " + parking.getFreeParkingBusPlaces());

                    }
                    break;
                case 2:
                    parking.getInformation();
                    break;
                case 3:
                    parking.clearBusParking();
                    parking.clearCarParking();
                    System.out.println("Parking is cleared");
                    System.out.println("Number of car places: " + parking.getFreeParkingBusPlaces());
                    System.out.println("Number of bus places" + parking.getFreeParkingCarPlaces());
                    break;
                default:
                    System.out.println("Wrong command!Please, try again.");
            }
            for (int i = 0; i < randomCars; i++) {
                car.get(i).setTime();
                if (car.get(i).getTime() == 0) {
                    parking.deleteCars(car.get(i));
                }
            }
            for (int i = 0; i < randomBuses; i++) {
                bus.get(i).setTime();
                if (bus.get(i).getTime() == 0) {
                    parking.deleteBuses(bus.get(i));
                }
            }
            System.out.println("Count of cars: " + parking.carCount());
            System.out.println("Count of buses: " + parking.busCount());
            System.out.println("Enter command to continue...");
        }
    }
}
