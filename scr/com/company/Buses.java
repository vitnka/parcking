package com.company;

public class Buses extends Places {
    int time;
    int busNumber;

    public Buses(int time, int busNumber) {
        this.time = time;
        this.busNumber = busNumber;
    }

    public int getTime() {
        return time;
    }

    public void setTime() {
        this.time --;
    }

    public int getBusNumber() {
        return busNumber;
    }

}
